(setq inhibit-startup-message t)

(use-package solarized-theme
  :ensure t
   :config (load-theme 'solarized-dark t))

;;(set-frame-parameter (selected-frame) 'alpha '(<active> . <inactive>))
;;(set-frame-parameter (selected-frame) 'alpha <both>)
(set-frame-parameter (selected-frame) 'alpha '(95 . 90))
(add-to-list 'default-frame-alist '(alpha . (95 . 90)))

(menu-bar-mode -1)
(tool-bar-mode -1)

(scroll-bar-mode -1)

(use-package nyan-mode
  :ensure t
  :config
  (nyan-mode))
'(nyan-wavy-trail t)

(global-hl-line-mode t)

(setq default-font-size 20)
(setq current-font-size default-font-size)
(setq font-change-increment 1.1)

(defalias 'list-buffers 'ibuffer-other-window)

(use-package counsel
:ensure t
  :bind
  (("M-y" . counsel-yank-pop)
   :map ivy-minibuffer-map
   ("M-y" . ivy-next-line)))

  (use-package ivy
  :ensure t
  :diminish (ivy-mode)
  :bind (("C-x b" . ivy-switch-buffer))
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "%d/%d ")
  (setq ivy-display-style 'fancy))

  (use-package swiper
  :ensure t
  :bind (("C-s" . swiper)
	 ("C-r" . swiper)
	 ("C-c C-r" . ivy-resume)
	 ("M-x" . counsel-M-x)
	 ("C-x C-f" . counsel-find-file))
  :config
  (progn
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t)
    (setq ivy-display-style 'fancy)
    (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
    ))

(use-package ace-window
:ensure t
:init
(progn
(global-set-key [remap other-window] 'ace-window)
(custom-set-faces
'(aw-leading-char-face
((t (:inherit ace-jump-face-foreground :height 3.0)))))
))

(use-package org-bullets
:ensure t
:config
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(use-package company
  :ensure t
  :config
  (add-hook 'after-init-hook 'global-company-mode))

(use-package neotree
  :ensure t
  :config
  (add-hook 'after-init-hook #'neotree-toggle))
(setq-default neo-show-hidden-files t)

(use-package git-gutter
  :ensure t
  :init
  (global-git-gutter-mode +1))

(use-package try
  :ensure t)

(use-package which-key
:ensure t
:config
(which-key-mode))
